## Login

### Fluxo Normal:

1. O utilizador fornece as credênciais: username/email e palavra-passe.
2. A aplicação verifica se existe um usuário com aquele username. 
3. A aplicação verifica se a palavra-passe correspondente àquele usuário é a
fornecida pelo usuário.

### Fluxo de Exceção (aquele username não identifica nenhum usuário registado) [Passo 3]:
1. A aplicação informa que o username introduzido não identifica nenhum usuário.

### Fluxo de Exceção (a palavra-passe introduzida não corresponde à correta) [Passo 3]:
1. A aplicação informa que a palavra-passe introduzida não corresponde á do username introduzido.
    

## LogOut

### Fluxo Normal:
1. O utilizador seleciona a opção de logout na aplicação. 
2. A aplicação interroga o usuário se este pretende realmente realizar logout. 
3. O utilizador confirma a intenção de realizar logout.
4. A aplicação acaba a sessão do usuário.

### Fluxo de Exceção (utilizador recua na intenção de realizar logout):
1. A aplicação cancela o logout.
2. A aplicação informa o usuário que o processo de logout foi cancelado.


## Registo de utilizador

### Fluxo Normal:
1. O utilizador fornece as credenciais de registo: username, email e password.
2. A aplicação confirma se existe algum usuário registado com o username ou email fornecido pelo usuário que se pretende registar. 
3. A aplicação regista o usuário, passando este a poder iniciar sessão com as credenciais que forneceu: username/email e password. 
4. A aplicação informa o usuário que o registo foi efetuado com sucesso.

Fluxo de Exceção (existe já algum utilizador registado com aquele email/username) [Passo 2]:
1. A aplicação informa o utilizador que o registo não se processou com sucesso devido ao facto de o email/username estar já em uso. 



## Eliminar um utilizador

### Fluxo Normal:
1. O utilizador informa a aplicação que pretende que a sua conta seja eliminada.
2. A aplicação interroga o utilizador se ele tem a certeza que pretende dar continuidade à operação.
3. O utilizador confirma a intenção de apagar a conta.
4. A aplicação pede ao utilizador para confirmar a palavra-passe.
5. O utilizador introduz a palavra-passe.
6. A aplicação confirma que a palavra-passe está correta.
7. A aplicação apaga a conta do utilizador.
8. A aplicação informa que a conta do utilizador foi removida com sucesso.

### Fluxo de Exceção (o utilizador não confirma a intenção de remover a conta) [Passo 3]:
1. A aplicação cancela o processo de remoção de conta. 
2. A aplicação informa o utilizador que o processo de remoção de conta foi cancelado.

### Fluxo Alternativo (a palavra-passe introduzida pelo usuário está incorreta) [Passo 6]:
1. A aplicação informa o usuário que a palavra-passe introduzida não foi a correta. 
2. Retoma o Fluxo Normal no passo 4.



## Criar um Workspace

### Fluxo Normal: 
1. O utilizador introduz o nome que quer dar ao Workspace.
2. A aplicação verifica se existe já algum Workspace com o nome introduzido.
3. O utilizador introduz um conjunto de skills que constituirão o skillset do workspace.
4. A aplicação cria o Workspace.
5. O utilizador convida um conjunto de utilizadores que pretende convidar para fazerem parte do workspace.

### Fluxo Alternativo (já existe algum Workspace com o nome introduzido) [Passo 2]:
1. A aplicação informa o utilizador que já existe um Workspace com o nome introduzido.
2. Retoma o Fluxo Normal no passo 1.



## Eliminar um Workspace

### Fluxo Normal:
1. A aplicação apresenta uma lista de Workspaces que são elimináveis pelo utilizador.
2. O utilizador escolhe eliminar um Workspace.
3. A aplicação questiona o utilizador se este tem a certeza de que pretende eliminar o Workspace.
4. O utilizador confirma a intenção de eliminar o Workspace.

### Fluxo de Exceção (o utilizador volta atrás na decisão de eliminar o Workspace) [Passo 5]:
1. A aplicação cancela o processo de remoção do Workspace.



## Adicionar utilizador ao Workspace

### Fluxo Normal:
1. O utilizador (dono do Workspace) adiciona um outro utilzador ao seu Workspace através de um email ou username.



## Remover utilizador do Workspace (na ótica do administrador)

### Fluxo Normal:
1. O administrador seleciona um utilizador para ser removido.
2. A aplicação pergunta se o administrador pretende, realmente, remover o utilizador do Workspace.
3. O administrador confirma a intenção de remover o outro utilizador.
    
### Fluxo de Exceção (a administrador recua na intenção de remover o utilizador do seu Workspace) [Passo 3]:
1. A aplicação cancela o processo de remoção do usuário.



## Remover utilizador do Workspace (na ótica do utilizador que quer deixar o Workspace)

### Fluxo Normal:
1. O utilizador seleciona a opção de deixar o Workspace.
2. A aplicação pergunta ao utilizador se, realmente, pretende abandonar o Workspace.
3. O utilizador confirma a intenção de abandonar o Workspace.

## Fluxo de Exceção (o utilizador recua na intenção de deixar o Workspace) [Passo 3]:
1. É cancelado o processo de abandono do Workspace.



## Especificar skills no Workspace

### Fluxo Normal:
1. A aplicação apresenta, ao utilizador que se juntou ao Workspace, uma lista de _skills_ que são valorizadas naquele Workspace.
2. O utilizador seleciona quais a skills que domina daquelas que foram apresentadas.



## Adicionar uma tarefa

### Fluxo Normal:
1. O utilzador introduz o conteúdo da tarefa.
2. O utilzador introduz uma duração estimada para a tarefa.
3. O utilizador atribui uma dificuldade à tarefa.
4. O utilizador indica se a tarefa é ou não periódica.



## Remover Tarefa (no sentido de elimina-la [Administrador])

### Fluxo Normal:
1. O administrador do Workspace seleciona uma tarefa e manifesta a intenção de a eliminar.
2. A aplicação questiona o utilizador se este tem a certeza que quer, realmente, eliminar a tarefa em questão. 
3. O administrador confirma a intençao de eliminar a tarefa.
4. A aplicação elimina a tarefa. 

## Fluxo de Exceção (o administrador recua na intenção de eliminar a tarefa) [Passo 3]:
1. O processo de remoção da tarefa é cancelado.
2. A aplicação informa o administrador que o processo de remoção da tarefa foi cancelado.



## Remover Tarefa (no sentido de marca-la como completa)

### Fluxo Normal:
1. O utilizador marca a tarefa como completa.



## Reportar um utilizador

### Fluxo Normal:
1. O utilizador introduz o username ou email do utilizador que pretende reportar. 
2. O utilizador explica a razão pela qual pretende reportar outro utilizador.
3. O utilizador seleciona a opção de submeter o report.
4. A aplicação pergunta ao utilizador se pretende realmente submeter o report.
5. O utilizador confirma a intenção de reportar outro utilizador.
6. A aplicação submete o report.

### Fluxo de Exceção (o utilizador recua na decisão de submeter o report a outro utilizador) [Passo 4]:
1. O processo de submissão do report é cancelado.
