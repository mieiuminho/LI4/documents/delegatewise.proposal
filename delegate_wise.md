# DelegateWise

- Título e Enquadramento

    O título é: **DelegateWise**.

    A ideia desta aplicação surgiu na medida em que sentimos necessidade de
    uma aplicação que implementasse a distribuição justa de tarefas num ambiente
    cooperativo, por exemplo: numa casa onde habitam diversas pessoas. 

    A necessidade da aplicação justifica-se na medida em que existem já
    diversas aplicações para a distribuição de tarefas em grupos organizados,
    como, por exemplo, o Trello. O software por nós desenvolvido diverge dos
    existentes na medida em que propomos a distribuição automática de tarefas
    mundanas com as quais os nossos usuários não queiram perder tempo a
    delegar.

- Objetivos

    A aplicação apresentada deve suportar a introdução de tarefas por parte
    de uma entidade que controla um conjunto de utilizadores (pode ser, por
    exemplo, um dos habitantes de um espaço partilhado). Uma vez introduzidas
    as tarefas estas devem ser divididas pela aplicação entre os
    participantes da "equipa" de forma equitativa.

    Assim, esta aplicação pretende implementar a distribuição justa de tarefas
    no seio de uma equipa eliminando quer o trabalho quer o tempo de distribuir
    as tarefas pelos diversos elementos disponíveis na equipa.

- Requisitos

    Existe um conjunto de requisitos que surgem como necessários: como
    pretendemos que cada utilizador seja atribuído a tarefas que tem aptidão
    para realizar torna-se necessário que os mesmo tenham associados um
    conjunto de habilidades (áreas de domínio, por exemplo: "design",
    "informática". 

    ### Utilizador

    - Um utilizador pode criar um espaço de trabalho (sendo que ele passa a ser
    o administrador desse mesmo espaço).
    - Pode adicionar tarefas
    - Convidar utilizadores para o espaço
    - Remover utilizadores do espaço
    - Se o utilizador tiver alguma ocupação, por exemplo, trabalhar ou for
    estudante: pode intrudizar o seu horário para que não lhe sejam
    atribuídas tarefas para _slots_ de horário que ele tenha já preenchidos
    pela sua ocupação. 

    ### Tarefas

    - Podem ter (facultativamente) uma periodicidade
    - Período limite de realização
    - Duração estimada
    - Área de domínio (o que permite que estas tarefas sejam idealmente
    atribuídas a utilizadores que dominem estas áreas). Esta é, no entanto,
    facultativa: uma tarefa que não tem associada uma área de domínio é uma
    tarefa que não necessita de trabalho especializado, pelo que pode ser
    atribuída a qualquer utilizador com igual probabilidade.
    - Contêm ainda um nível de dificuldade que é tido em consideração na
    atribuição das mesmas, assim como o tempo estimado para a realização das
    mesmas (estes dois aspetos são considerados no balanceamento das tarefas).

- Tecnologias

    Pretendemos desenvolver uma aplicação web pelo que serão necessários conhecimentos de HTML, CSS e JavaScript (em específico da framework React.js). A interação com a aplicação por parte do utilizador gera pedidos que serão enviados para o servidor (que será escrito em Java) e tratados utilizando a framework Spring que será ainda capaz de estabelecer a comunicação com a base de dados.
    Por último teremos o backoffice da aplicação em C# que permitirá ter estatísticas relativas, por exemplo, ao número de utilizadores registados na aplicação.

- Cronograma
